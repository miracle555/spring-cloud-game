package com.douqu.game.plantwar.zuul.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

/**
 * zuul的过滤器
 */
@Component
public class PreRequestFilter extends ZuulFilter {

    @Override
    /**
     * 声明是pre类型
     * pre过滤器 route过滤器 post过滤器
     */
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    /** 返回值越小表示优先级越高 */
    public int filterOrder() {
        return 0;
    }

    @Override
    /** 返回true表示需要过滤 返回false表示不需要过滤 */
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //后期这里 处理登录和充值的接口
        return null;
    }
}
