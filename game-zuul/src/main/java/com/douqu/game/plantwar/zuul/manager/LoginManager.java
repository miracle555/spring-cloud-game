package com.douqu.game.plantwar.zuul.manager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 拦截处理登录请求
 * 如果同时登录的玩家太多则通知玩家请稍后重试
 */
public class LoginManager {

    private AtomicInteger loginTimes = new AtomicInteger(0);

    private static LoginManager instance;

    public static LoginManager getInstance(){
        if (instance == null){
            synchronized (LoginManager.class){
                if (instance == null)
                    instance = new LoginManager();
            }
        }
        return instance;
    }

    public boolean isCanLogin(){
        //同时登录的玩家不能超过50人
        if (loginTimes.get() >= 50){
            return false;
        }
        loginTimes.addAndGet(1);
        return true;
    }

    public void loginEnd(){
        loginTimes.addAndGet(-1);
    }
}
