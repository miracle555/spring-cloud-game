package com.douqu.game.plantwar.main.exception;

import com.douqu.game.plantwar.core.commons.Msg;
import com.douqu.game.plantwar.core.commons.ReturnCode;
import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class ExceptionController {

    /**
     * 异常不返回 400 500 捕获优雅的提示
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    public Mono serverExceptionHandler(Exception e){
        Msg msg = new Msg();
        if (e instanceof TokenException){
            LogUtils.error(" RunTimeException token error ->" + TokenException.class.getSimpleName());
            msg.setCode(ReturnCode.RETURN_LOGIN);
            msg.setMsg("token过期 请重新登陆");
        }else{
            msg.setCode(ReturnCode.MICRO_SERVICE_UNAVAILABLE);
            msg.setMsg("微服务不可用，请稍后重试");
            e.printStackTrace();
        }
        return Mono.just(msg);
    }

}
