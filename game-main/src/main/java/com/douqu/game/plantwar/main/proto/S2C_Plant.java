package com.douqu.game.plantwar.main.proto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("种植种子")
public class S2C_Plant {

    @ApiModelProperty(value = "土地id")
    private int farmId;
    @ApiModelProperty(value = "种子id")
    private int plantId;
    @ApiModelProperty(value =  "种植数量")
    private int count;
    @ApiModelProperty(value = "种植时间")
    private long plantTime;

    public int getFarmId() {
        return farmId;
    }

    public void setFarmId(int farmId) {
        this.farmId = farmId;
    }

    public int getPlantId() {
        return plantId;
    }

    public void setPlantId(int plantId) {
        this.plantId = plantId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getPlantTime() {
        return plantTime;
    }

    public void setPlantTime(long plantTime) {
        this.plantTime = plantTime;
    }
}
