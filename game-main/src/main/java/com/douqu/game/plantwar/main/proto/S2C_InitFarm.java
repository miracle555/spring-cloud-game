package com.douqu.game.plantwar.main.proto;

import com.douqu.game.plantwar.core.commons.GoodData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

@ApiModel("初始化农场")
public class S2C_InitFarm {

    @ApiModelProperty(value = "培育信息")
    private List<GoodData> trainInfo;
    @ApiModelProperty(value = "种子信息")
    private List<GoodData> seedInfo;
    @ApiModelProperty(value = "培育中心等级")
    private int trainCenterLevel;
    @ApiModelProperty("仓库等级")
    private int warehouseLevel;
    @ApiModelProperty("仓库信息")
    private List<GoodData> warehouseInfo;
    @ApiModelProperty("农田信息")
    private List<GoodData> farmlandInfo;

    public List<GoodData> getTrainInfo() {
        return trainInfo;
    }

    public void setTrainInfo(List<GoodData> trainInfo) {
        this.trainInfo = trainInfo;
    }

    public List<GoodData> getSeedInfo() {
        return seedInfo;
    }

    public void setSeedInfo(List<GoodData> seedInfo) {
        this.seedInfo = seedInfo;
    }

    public int getTrainCenterLevel() {
        return trainCenterLevel;
    }

    public void setTrainCenterLevel(int trainCenterLevel) {
        this.trainCenterLevel = trainCenterLevel;
    }

    public int getWarehouseLevel() {
        return warehouseLevel;
    }

    public void setWarehouseLevel(int warehouseLevel) {
        this.warehouseLevel = warehouseLevel;
    }

    public List<GoodData> getWarehouseInfo() {
        return warehouseInfo;
    }

    public void setWarehouseInfo(List<GoodData> warehouseInfo) {
        this.warehouseInfo = warehouseInfo;
    }

    public List<GoodData> getFarmlandInfo() {
        return farmlandInfo;
    }

    public void setFarmlandInfo(List<GoodData> farmlandInfo) {
        this.farmlandInfo = farmlandInfo;
    }
}
