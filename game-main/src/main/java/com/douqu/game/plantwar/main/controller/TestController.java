package com.douqu.game.plantwar.main.controller;

import com.bean.core.util.Utils;
import com.douqu.game.plantwar.core.TestObj;
import com.douqu.game.plantwar.core.commons.AppConstant;
import com.douqu.game.plantwar.main.exception.TokenException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/test")
    public Mono test(ServerWebExchange exchange){
        Long roleId = exchange.getAttribute(AppConstant.ROLE_ID_PREX);
        String account = exchange.getAttribute(AppConstant.ACCOUNT);
        if (roleId == null || Utils.isNullOrEmpty(account)){
            throw new TokenException();
        }
        TestObj testObj = new TestObj();
        System.out.println(roleId);
        System.out.println(account);
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        testObj.setId(1);
        testObj.setAccount("2sdfasdadasddxsa");
        testObj.setHead("52d4f5sa41f5d4");
        testObj.setName("hello world");
        List<String> list = new ArrayList<>();
        list.add("655sad");
        list.add("dsdsda");
        testObj.setParam(list);
        return Mono.just(testObj);
    }
}
