package com.douqu.game.plantwar.main.controller;

import com.douqu.game.plantwar.core.commons.AppConstant;
import com.douqu.game.plantwar.core.commons.GoodData;
import com.douqu.game.plantwar.core.commons.Msg;
import com.douqu.game.plantwar.core.commons.ReturnCode;
import com.douqu.game.plantwar.core.manager.FarmManager;
import com.douqu.game.plantwar.core.manager.FarmWatch;
import com.douqu.game.plantwar.core.utils.CoreUtils;
import com.douqu.game.plantwar.core.utils.LogUtils;
import com.douqu.game.plantwar.main.exception.TokenException;
import com.douqu.game.plantwar.main.proto.S2C_InitFarm;
import com.douqu.game.plantwar.main.proto.S2C_Plant;
import com.douqu.game.plantwar.main.redis.RedisConfig;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/farm")
public class FarmController {

    @Autowired
    private RedisConfig redisConfig;


    @RequestMapping("/initFarm")
    /**
     * 农场初始化
     */
    public Mono initFarm(ServerWebExchange exchange){
        Long roleId = exchange.getAttribute(AppConstant.ROLE_ID_PREX);
        if (roleId == null){
            throw new TokenException();
        }
        FarmManager farmManager = (FarmManager) redisConfig.getValue(roleId, FarmManager.class);
        if (farmManager == null){
            throw new TokenException();
        }

        S2C_InitFarm response = new S2C_InitFarm();
        response.setFarmlandInfo(farmManager.getFarmlandInfo());
        response.setSeedInfo(farmManager.getSeedInfo());
        response.setTrainCenterLevel(farmManager.getTrainCenterLevel());
        response.setTrainInfo(farmManager.getTrainInfo());
        response.setWarehouseInfo(farmManager.getWarehouseInfo());
        response.setWarehouseLevel(farmManager.getWarehouseLevel());
        Msg msg = new Msg();
        msg.setCode(ReturnCode.SUCCESS);
        msg.setData(response);
        return Mono.just(msg);
    }

    @RequestMapping("/plant")
    /**
     * 这里的plant 是动词 种植的意思
     */
    public Mono plant(ServerWebExchange exchange,final int farmId, final int plantId,final int count){
        Long roleId = exchange.getAttribute(AppConstant.ROLE_ID_PREX);
        if (roleId == null){
            throw new TokenException();
        }
        FarmManager farmManager = (FarmManager) redisConfig.getValue(roleId, FarmManager.class);
        if (farmManager == null){
            throw new TokenException();
        }
        LogUtils.info("种植土地id:" + farmId + " 种植种子id：" + plantId + " 种植种子数量：" + count);
        Msg msg = new Msg();
        //1.验证土地是否存在
        GoodData farmData = farmManager.getFarmlandInfo().get(farmId);
        if (farmData == null){
            msg.setCode(ReturnCode.FARM_LOCK);
            msg.setMsg("小哥哥加油哦，这块土地还未解锁");
            LogUtils.error("这块土地还未解锁 farmId ->" + farmId);
            return Mono.just(msg);
        }
        //2.验证土地是否有种植
        //TODO
        //获取要种植的种子
        GoodData seedData = CoreUtils.getByList(farmManager.getSeedInfo(),new GoodData(plantId));
        //3.验证种子是否存在
        if (seedData == null){
            msg.setCode(ReturnCode.SEED_NOT_EXIST);
            msg.setMsg("您还未获得这个种子");
            LogUtils.error("没有该种子 plantId ->" + plantId);
            return Mono.just(msg);
        }
        //4.验证种子的数量是否足够
        if (seedData.getId() < count){
            msg.setCode(ReturnCode.SEED_LAZY_WEIGHT);
            msg.setMsg("种子数量不足");
            LogUtils.error("拥有数量：" + seedData.getId());
        }
        //种植

        FarmWatch.plant(farmData,plantId,count);
        redisConfig.setValue(roleId,farmManager);
        S2C_Plant result = new S2C_Plant();
        result.setFarmId(farmId);
        result.setPlantId(plantId);
        result.setCount(count);
        result.setPlantTime(farmData.getVaue());
        msg.setData(result);
        return Mono.just(msg);
    }


}
