package com.douqu.game.plantwar.core.manager;

import com.douqu.game.plantwar.core.commons.BaseEntity;
import com.douqu.game.plantwar.core.commons.GoodData;

import java.util.List;

public class PastureManager extends BaseEntity {

    private int id;
    //玩家id
    private long roleId;

    //牧场不需要知道每个房子中宠物的状态与数量 这些数据读取配置表实时获取
    //type 房子状态 id 房子等级 value 房子开始建设时间
    private List<GoodData> houseInfo;

}
