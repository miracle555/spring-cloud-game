package com.douqu.game.plantwar.core.commons;

/**
 * ReturnCode
 */
public class ReturnCode {
    /** http请求返回码 */
    public static final int OK = 200;
    public static final int BAD_REQUEST = 400;
    public static final int FORBIDDEN = 403;
    public static final int NOT_FOUNT = 404;
    public static final int MICRO_SERVICE_UNAVAILABLE = 40001;//微服务不可用
    public static final int SERVER_ERROR=500;
    public static final int BAD_GATEWAY = 502;
    public static final int SERVICE_UNAVAILABLE=503;

    /** 游戏错误码 */
    public static final int SUCCESS = 10000;
    /** 参数异常 */
    public static final int PARAM_ERROR = 10001;
    /** token过期请重新登陆 */
    public static final int RETURN_LOGIN = 10002;

    /** 农场 11000 **/
    /** 种子不存在 */
    public static final int SEED_NOT_EXIST = 11001;
    /** 种子数量不足 */
    public static final int SEED_LAZY_WEIGHT = 11002;
    /** 这块地已经有种子 */
    public static final int FARM_HAS_PLANT = 11003;
    /** 这块土地还未解锁 */
    public static final int FARM_LOCK = 11004;

}
