package com.douqu.game.plantwar.core.utils;

import java.util.List;

public class CoreUtils {

    public static<T> T getByList(List<T> list,T t){
        if (listIsNull(list))
            return null;
        int index = list.indexOf(t);
        if (index < 0)
            return null;
        return list.get(index);
    }


    public static<T> boolean listIsNull(List<T> list){
        return list == null || list.isEmpty();
    }
}
