package com.douqu.game.plantwar.core.factory;

public enum E_Platform {

    UNKNOWN(0, "无"),
    WECHAT(1,"微信"),
    CRAZY_GAME(2,"疯狂游戏"),
    I_PLAY(3, "我玩"),
    UC(4, "九游"),
    BAIDU(5,"百度"),
    FOUR399(6,"4399")
    ;

    int code;

    String msg;

    E_Platform(int code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static E_Platform forNumber(int platform)
    {
        for(E_Platform item : values())
        {
            if(item.getCode() == platform)
                return item;
        }
        return UNKNOWN;
    }
}
