package com.douqu.game.plantwar.core.manager;

import com.douqu.game.plantwar.core.model.RoleInfo;

public class RoleWatch{

    public static RoleInfo initRoleInfo(String account,String head,String nickName,int platform,long parentId){
        RoleInfo roleInfo = new RoleInfo();
        roleInfo.setAccount(account);
        roleInfo.setHead(head);
        roleInfo.stringToNickName(nickName);
        roleInfo.setPlatform(platform);
        roleInfo.setParentId(parentId);
        roleInfo.setLevel(1);
        roleInfo.setExp(0);
        return roleInfo;
    }

    public static RoleInfo toInfo(RoleManager roleManager) {
        RoleInfo roleInfo = new RoleInfo();
        roleInfo.setExp(roleManager.getExp());
        roleInfo.setLevel(roleManager.getLevel());
        roleInfo.setParentId(roleManager.getParentId());
        roleInfo.setPlatform(roleManager.getPlatform());
        roleInfo.stringToNickName(roleManager.getNickName());
        roleInfo.setHead(roleManager.getHead());
        roleInfo.setAccount(roleManager.getAccount());
        roleInfo.setId(roleManager.getId());
        return roleInfo;
    }

    public static RoleManager toManager(RoleInfo roleInfo) {
        RoleManager roleManager = new RoleManager();
        roleManager.setAccount(roleInfo.getAccount());
        roleManager.setExp(roleInfo.getExp());
        roleManager.setHead(roleInfo.getHead());
        roleManager.setId(roleInfo.getId());
        roleManager.setLevel(roleInfo.getLevel());
        roleManager.setNickName(roleInfo.nickNameToString());
        roleManager.setParentId(roleInfo.getParentId());
        roleManager.setPlatform(roleInfo.getPlatform());
        return roleManager;
    }
}
