package com.douqu.game.plantwar.core.manager;

import com.douqu.game.plantwar.core.commons.BaseEntity;
import com.douqu.game.plantwar.core.commons.GoodData;

import java.util.List;

/**
 * 农场
 */
public class FarmManager extends BaseEntity {

    private long id;
    //培育中心等级
    private int trainCenterLevel;
    //培育信息 type种子id id种子数量 value开始培育时间
    private List<GoodData> trainInfo;
    //种子信息 type种子id id种子value
    private List<GoodData> seedInfo;
    //仓库等级
    private int warehouseLevel;
    //仓库信息 type植物id id植物数量
    private List<GoodData> warehouseInfo;
    //农田信息 list的序号表示地的序号 type植物id id数量 vlue种植时间
    private List<GoodData> farmlandInfo;
    //玩家id
    private long roleId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTrainCenterLevel() {
        return trainCenterLevel;
    }

    public void setTrainCenterLevel(int trainCenterLevel) {
        this.trainCenterLevel = trainCenterLevel;
    }

    public List<GoodData> getTrainInfo() {
        return trainInfo;
    }

    public void setTrainInfo(List<GoodData> trainInfo) {
        this.trainInfo = trainInfo;
    }

    public List<GoodData> getSeedInfo() {
        return seedInfo;
    }

    public void setSeedInfo(List<GoodData> seedInfo) {
        this.seedInfo = seedInfo;
    }

    public int getWarehouseLevel() {
        return warehouseLevel;
    }

    public void setWarehouseLevel(int warehouseLevel) {
        this.warehouseLevel = warehouseLevel;
    }

    public List<GoodData> getWarehouseInfo() {
        return warehouseInfo;
    }

    public void setWarehouseInfo(List<GoodData> warehouseInfo) {
        this.warehouseInfo = warehouseInfo;
    }

    public List<GoodData> getFarmlandInfo() {
        return farmlandInfo;
    }

    public void setFarmlandInfo(List<GoodData> farmlandInfo) {
        this.farmlandInfo = farmlandInfo;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
