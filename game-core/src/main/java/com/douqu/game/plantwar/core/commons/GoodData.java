package com.douqu.game.plantwar.core.commons;

import java.util.Objects;

public class GoodData extends BaseEntity {

    private int type;
    private int id;
    private long vaue;

    public GoodData(int type,int id,long value){
        this.type = type;
        this.id = id;
        this.vaue = value;
    }

    public GoodData(int type){
        this.type = type;
    }

    public GoodData(){

    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getVaue() {
        return vaue;
    }

    public void setVaue(long vaue) {
        this.vaue = vaue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodData goodData = (GoodData) o;
        return type == goodData.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
