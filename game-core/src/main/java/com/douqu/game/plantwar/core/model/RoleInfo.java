package com.douqu.game.plantwar.core.model;

import com.douqu.game.plantwar.core.commons.BaseEntity;
import org.bouncycastle.util.encoders.UrlBase64;

import java.util.Date;

public class RoleInfo extends BaseEntity {

    private long id;
    private String account;
    private String head;
    private byte[] nickName;
    private int level;
    private int exp;
    private int platform;
    private long parentId;
    private Date lastLoginTime;
    private Date createTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public byte[] getNickName() {
        return nickName;
    }

    public void setNickName(byte[] nickName) {
        this.nickName = nickName;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String nickNameToString(){
        if (nickName == null || nickName.length == 0){
            return "";
        }
        byte[] array = UrlBase64.decode(nickName);
        return new String(array);
    }

    public void stringToNickName(String name){
        this.nickName = UrlBase64.encode(name.getBytes());
    }

    @Override
    public String toString() {
        return "RoleInfo{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", head='" + head + '\'' +
                ", nickName=" + nickNameToString() +
                ", level=" + level +
                ", exp=" + exp +
                ", platform=" + platform +
                ", parentId=" + parentId +
                '}';
    }
}
