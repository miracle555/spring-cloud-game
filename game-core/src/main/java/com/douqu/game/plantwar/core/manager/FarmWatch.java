package com.douqu.game.plantwar.core.manager;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.douqu.game.plantwar.core.commons.GoodData;
import com.douqu.game.plantwar.core.model.FarmInfo;

import java.util.ArrayList;
import java.util.List;

public class FarmWatch {

    public static FarmInfo initFarmInfo(long roleId){
        FarmInfo farmInfo = new FarmInfo();
        farmInfo.setTrainCenterLevel(1);
        farmInfo.setTrainInfo(JSONObject.toJSONString(new ArrayList<>()));
        farmInfo.setSeedInfo(JSONObject.toJSONString(new ArrayList<>()));
        farmInfo.setWarehouseLevel(1);
        farmInfo.setWarehouseInfo(JSONObject.toJSONString(new ArrayList<>()));
        farmInfo.setFarmlandInfo(JSONObject.toJSONString(new ArrayList<>()));
        farmInfo.setRoleId(roleId);
        return farmInfo;
    }

    public static FarmInfo toInfo(FarmManager farmManager) {
        FarmInfo farmInfo = new FarmInfo();
        farmInfo.setId(farmManager.getId());
        farmInfo.setTrainCenterLevel(farmInfo.getTrainCenterLevel());
        farmInfo.setTrainInfo(JSONObject.toJSONString(farmManager.getTrainInfo()));
        farmInfo.setSeedInfo(JSONObject.toJSONString(farmManager.getSeedInfo()));
        farmInfo.setWarehouseLevel(farmManager.getWarehouseLevel());
        farmInfo.setWarehouseInfo(JSONObject.toJSONString(farmManager.getWarehouseInfo()));
        farmInfo.setFarmlandInfo(JSONObject.toJSONString(farmManager.getFarmlandInfo()));
        farmInfo.setRoleId(farmManager.getRoleId());
        return farmInfo;
    }

    public static FarmManager toManager(FarmInfo farmInfo) {
        FarmManager farmManager = new FarmManager();
        farmManager.setId(farmInfo.getId());
        farmManager.setTrainCenterLevel(farmInfo.getTrainCenterLevel());
        farmManager.setTrainInfo(JSON.parseObject(farmInfo.getTrainInfo(), new TypeReference<List<GoodData>>(){}));
        farmManager.setSeedInfo(JSON.parseObject(farmInfo.getSeedInfo(), new TypeReference<List<GoodData>>(){}));
        farmManager.setWarehouseLevel(farmInfo.getWarehouseLevel());
        farmManager.setWarehouseInfo(JSON.parseObject(farmInfo.getWarehouseInfo(), new TypeReference<List<GoodData>>(){}));
        farmManager.setFarmlandInfo(JSON.parseObject(farmInfo.getFarmlandInfo(), new TypeReference<List<GoodData>>(){}));
        farmManager.setRoleId(farmInfo.getRoleId());
        return farmManager;
    }

    public static void plant(GoodData goodData,final int plantId,final int count){
        goodData.setType(plantId);
        goodData.setId(count);
        goodData.setVaue(System.currentTimeMillis());
    }
}
