package com.douqu.game.plantwar.core.commons;

import java.io.Serializable;

public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    //在定时同步mysql的时候 检测是否有数据改变 如果为false则不浪费性能同步数据
    protected boolean update = false;

    public boolean getUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }
}
