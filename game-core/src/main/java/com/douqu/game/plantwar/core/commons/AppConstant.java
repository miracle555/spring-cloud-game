package com.douqu.game.plantwar.core.commons;

public class AppConstant {

    /** token中roleId的前缀 */
    public static final String ROLE_ID_PREX = "roleId";
    public static final String ROLE_PREX = "role";
    public static final String ACCOUNT = "account";
    public static final String HEAD = "head";
    public static final String NICK_NAME = "nickName";

    public static final String GAME_NAME = "plantwar";
    public static final String UNDERLINE = "_";

    public static<T> String getKey(T prefix, Class c){
        return GAME_NAME + UNDERLINE + c.getSimpleName() + UNDERLINE + prefix;
    }
}
