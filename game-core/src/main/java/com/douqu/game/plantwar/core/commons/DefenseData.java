package com.douqu.game.plantwar.core.commons;

import java.util.List;

/**
 * 防守日志
 */
public class DefenseData extends BaseEntity {

    //敌人id
    private long enemyId;
    //敌人名称
    private String enemyName;
    //对手损耗信息
    private List<GoodData> enemyLossInfo;
    //防守状态 成功或失败
    private boolean defenseStatus;
    //对手奖励信息
    private List<GoodData> rewardInfo;
    //复仇状态
    private boolean revengeStatus;

    public long getEnemyId() {
        return enemyId;
    }

    public void setEnemyId(long enemyId) {
        this.enemyId = enemyId;
    }

    public String getEnemyName() {
        return enemyName;
    }

    public void setEnemyName(String enemyName) {
        this.enemyName = enemyName;
    }

    public List<GoodData> getEnemyLossInfo() {
        return enemyLossInfo;
    }

    public void setEnemyLossInfo(List<GoodData> enemyLossInfo) {
        this.enemyLossInfo = enemyLossInfo;
    }

    public boolean isDefenseStatus() {
        return defenseStatus;
    }

    public void setDefenseStatus(boolean defenseStatus) {
        this.defenseStatus = defenseStatus;
    }

    public List<GoodData> getRewardInfo() {
        return rewardInfo;
    }

    public void setRewardInfo(List<GoodData> rewardInfo) {
        this.rewardInfo = rewardInfo;
    }

    public boolean isRevengeStatus() {
        return revengeStatus;
    }

    public void setRevengeStatus(boolean revengeStatus) {
        this.revengeStatus = revengeStatus;
    }
}
