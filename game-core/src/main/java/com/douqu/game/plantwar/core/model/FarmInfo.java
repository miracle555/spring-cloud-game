package com.douqu.game.plantwar.core.model;

public class FarmInfo{

    private long id;
    //培育中心等级
    private int trainCenterLevel;
    //培育信息
    private String trainInfo;
    //种子信息
    private String seedInfo;
    //仓库等级
    private int warehouseLevel;
    //仓库信息
    private String warehouseInfo;
    //农田信息
    private String farmlandInfo;
    //玩家id
    private long roleId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTrainCenterLevel() {
        return trainCenterLevel;
    }

    public void setTrainCenterLevel(int trainCenterLevel) {
        this.trainCenterLevel = trainCenterLevel;
    }

    public String getTrainInfo() {
        return trainInfo;
    }

    public void setTrainInfo(String trainInfo) {
        this.trainInfo = trainInfo;
    }

    public String getSeedInfo() {
        return seedInfo;
    }

    public void setSeedInfo(String seedInfo) {
        this.seedInfo = seedInfo;
    }

    public int getWarehouseLevel() {
        return warehouseLevel;
    }

    public void setWarehouseLevel(int warehouseLevel) {
        this.warehouseLevel = warehouseLevel;
    }

    public String getWarehouseInfo() {
        return warehouseInfo;
    }

    public void setWarehouseInfo(String warehouseInfo) {
        this.warehouseInfo = warehouseInfo;
    }

    public String getFarmlandInfo() {
        return farmlandInfo;
    }

    public void setFarmlandInfo(String farmlandInfo) {
        this.farmlandInfo = farmlandInfo;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
