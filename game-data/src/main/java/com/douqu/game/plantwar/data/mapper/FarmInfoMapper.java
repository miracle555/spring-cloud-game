package com.douqu.game.plantwar.data.mapper;

import com.douqu.game.plantwar.core.model.FarmInfo;

import java.io.IOException;

public interface FarmInfoMapper {

    int insertFarm(FarmInfo farmInfo) throws IOException;

    int updateFarm(FarmInfo farmInfo) throws IOException;

    FarmInfo selectByRoleId(Long id) throws IOException;

}
