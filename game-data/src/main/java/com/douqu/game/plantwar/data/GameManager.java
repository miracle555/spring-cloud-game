package com.douqu.game.plantwar.data;

import com.douqu.game.plantwar.core.commons.BaseEntity;
import com.douqu.game.plantwar.data.mapper.FarmInfoMapper;
import com.douqu.game.plantwar.data.mapper.RoleInfoMapper;
import com.douqu.game.plantwar.data.redis.RedisConfig;
import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.swing.text.html.parser.Entity;
import java.util.Map;

@Component
public class GameManager {

    //TODO
    /**
     * 这里应该拆出来 另外起一个项目来做
     * 因为auth要启多个 会产生重复数据存储
     */


    private static ThreadPoolTaskExecutor executor;

    @Autowired
    private FarmInfoMapper farmInfoMapper;
    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Autowired
    private RedisConfig redisConfig;

    private final static int HOUR_TIMER = 60 * 60 * 1000;
    //private final static int TIME_LAZY = 0 * 60 * 1000;
    private final static int TEN_MINUTES = 1 * 60 * 1000;

    @Scheduled(fixedRate = TEN_MINUTES)
    public void asyncSavePlayerCache(){
        //每10分钟执行一次 同步玩家数据到数据库
        LogUtils.info("每10分钟执行一次 定时任务");
        Map<String, BaseEntity> map = redisConfig.getAll();

        for (Map.Entry<String,BaseEntity> entry:map.entrySet()){
            if (entry.getValue().getUpdate()){
                entry.getValue().setUpdate(false);
                redisConfig.setValue(entry.getKey(),entry.getValue());
            }
        }

        LogUtils.info("执行成功 哈哈哈");
    }

    public static ThreadPoolTaskExecutor getExecutor() {
        return executor;
    }

    public static void setExecutor(ThreadPoolTaskExecutor executor) {
        GameManager.executor = executor;
    }
}
