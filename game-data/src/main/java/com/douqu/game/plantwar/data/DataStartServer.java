package com.douqu.game.plantwar.data;

import com.douqu.game.plantwar.core.utils.LogUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@MapperScan("com.douqu.game.plantwar.data.mapper")
public class DataStartServer {

    public static void main(String[] args) {
        SpringApplication.run(DataStartServer.class,args);
    }

    @Configuration
    @EnableAsync
    class AsyncTask implements SchedulingConfigurer {

        private int corePoolSize = 1;
        private int maxPoolSize = 10;
        private int queueCapacity = 100;

        @Bean
        public Executor taskExecutor(){

            LogUtils.info("成功注入线程池");
            ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
            executor.setCorePoolSize(corePoolSize);
            executor.setMaxPoolSize(maxPoolSize);
            executor.setQueueCapacity(queueCapacity);
            executor.initialize();
            GameManager.setExecutor(executor);
            return executor;
        }

        @Override
        public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
            taskRegistrar.setScheduler(Executors.newScheduledThreadPool(5));
        }
    }
}
