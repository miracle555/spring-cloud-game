package com.douqu.game.plantwar.auth.interceptor;

import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 拦截器 验证token如果未进行auth认证 进行操作视为非法操作
 */
public class AuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        String url = request.getRequestURI();
        LogUtils.info("utl ->" + url);

        if (url.equals("/auth")){

        }else {

        }
        return true;
    }
}
