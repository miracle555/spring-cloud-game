package com.douqu.game.plantwar.auth.service;

import com.douqu.game.plantwar.core.commons.Msg;

public interface RoleService {

    Msg auth(String account,String head,String nickName,
             int platform,long parentId) throws Exception;
}
