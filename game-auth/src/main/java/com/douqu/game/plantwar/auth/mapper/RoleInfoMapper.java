package com.douqu.game.plantwar.auth.mapper;

import com.douqu.game.plantwar.core.model.RoleInfo;

import java.io.IOException;

public interface RoleInfoMapper {

    int insert(RoleInfo roleInfo) throws IOException;

    RoleInfo selectByPrimaryKey(Long id) throws IOException;

    RoleInfo selectByAccount(String account) throws IOException;

    int update(RoleInfo roleInfo) throws IOException;
}
