package com.douqu.game.plantwar.auth.service;

import com.bean.core.util.Utils;
import com.douqu.game.plantwar.auth.mapper.FarmInfoMapper;
import com.douqu.game.plantwar.auth.mapper.RoleInfoMapper;
import com.douqu.game.plantwar.auth.redis.RedisConfig;
import com.douqu.game.plantwar.core.commons.Msg;
import com.douqu.game.plantwar.core.commons.ReturnCode;
import com.douqu.game.plantwar.core.factory.E_Platform;
import com.douqu.game.plantwar.core.manager.FarmWatch;
import com.douqu.game.plantwar.core.manager.RoleWatch;
import com.douqu.game.plantwar.core.model.FarmInfo;
import com.douqu.game.plantwar.core.model.RoleInfo;
import com.douqu.game.plantwar.core.utils.JWTUtils;
import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RedisConfig redisConfig;

    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Autowired
    private FarmInfoMapper farmInfoMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Msg auth(String account, String head, String nickName, int platform, long parentId) throws Exception {
        Msg msg = new Msg();
        if (Utils.isNullOrEmpty(account)) {
            msg.setCode(ReturnCode.PARAM_ERROR);
            LogUtils.error("account is null");
            return msg;
        }
        if (Utils.isNullOrEmpty(head)){
            msg.setCode(ReturnCode.PARAM_ERROR);
            LogUtils.error("head is null");
            return msg;
        }
        if (Utils.isNullOrEmpty(nickName)){
            msg.setCode(ReturnCode.PARAM_ERROR);
            LogUtils.error("nickName is null");
            return msg;
        }
        if (platform == 0){
            if (!account.startsWith("test")){
                msg.setCode(ReturnCode.PARAM_ERROR);
                LogUtils.error("platform error platform ->" + platform);
                return msg;
            }
            //内部测试账号 platform是0  以test开头
        }
        LogUtils.info("account :" + account + " head:" + head + " nickName:" + nickName + " platform:" + platform + " parentId:" + parentId);
        if (platform == E_Platform.WECHAT.getCode()){
            //微信
        }
        RoleInfo roleInfo = roleInfoMapper.selectByAccount(account);
        FarmInfo farmInfo = null;
        if (roleInfo == null){
            //玩家不存在 创建新玩家
            roleInfo = RoleWatch.initRoleInfo(account,head,nickName,platform,parentId);
            //TODO parentId未处理
            roleInfoMapper.insert(roleInfo);
            farmInfo = FarmWatch.initFarmInfo(roleInfo.getId());
            farmInfoMapper.insertFarm(farmInfo);
        }else{
            farmInfo = farmInfoMapper.selectByRoleId(roleInfo.getId());
            if (farmInfo == null){
                farmInfo = FarmWatch.initFarmInfo(roleInfo.getId());
            }
        }
        //缓存到redis里面  redis的id一定不能写错 不然会造成串改别的玩家的数据
        redisConfig.setValue(roleInfo.getId(),RoleWatch.toManager(roleInfo));
        redisConfig.setValue(roleInfo.getId(),FarmWatch.toManager(farmInfo));

        msg.setCode(ReturnCode.SUCCESS);
        msg.setMsg("认证登陆成功!");
        msg.setData(JWTUtils.createToken(roleInfo.getId(),account,head,nickName));
        LogUtils.info("认证登陆成功 " + roleInfo.toString());
        return msg;
    }
}
