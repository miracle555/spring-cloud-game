package com.douqu.game.plantwar.auth.controller;

import com.douqu.game.plantwar.auth.service.RoleService;
import com.douqu.game.plantwar.core.commons.Msg;
import com.douqu.game.plantwar.core.commons.ReturnCode;
import com.douqu.game.plantwar.core.utils.LogUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/")
public class LoginController {

    @Autowired
    private RoleService roleService;

    /**
     * 认证也就是登录 返回一个token
     * @param account
     * @param head
     * @param nickName
     * @param platform
     * @param parentId
     * @param request
     * @return
     */
    @ApiOperation("认证")
    @RequestMapping(value = "/auth",method = RequestMethod.POST)
    public Msg auth(@RequestParam String account,@RequestParam String head,@RequestParam String nickName,
                    @RequestParam int platform,@RequestParam long parentId,@ApiIgnore() HttpServletRequest request){
        Msg msg = new Msg();
        msg.setCode(ReturnCode.PARAM_ERROR);
        try {
            msg = roleService.auth(account,head,nickName,platform,parentId);
        }catch (Exception e){
            e.printStackTrace();
            LogUtils.errorException(e);
        }
        return msg;
    }
}
