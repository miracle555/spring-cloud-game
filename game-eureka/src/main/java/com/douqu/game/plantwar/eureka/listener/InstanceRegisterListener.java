package com.douqu.game.plantwar.eureka.listener;

import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceRegisteredEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InstanceRegisterListener implements ApplicationListener<EurekaInstanceRegisteredEvent> {
    @Override
    public void onApplicationEvent(EurekaInstanceRegisteredEvent event) {
        LogUtils.info("服务：" + event.getInstanceInfo().getAppName() + "，注册成功了");
    }
}
