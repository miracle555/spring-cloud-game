package com.douqu.game.plantwar.eureka.listener;

import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceRenewedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InstanceRenewListener implements ApplicationListener<EurekaInstanceRenewedEvent> {
    @Override
    public void onApplicationEvent(EurekaInstanceRenewedEvent event) {
        LogUtils.info("心跳检测服务 ：" + event.getInstanceInfo().getAppName());
    }
}
