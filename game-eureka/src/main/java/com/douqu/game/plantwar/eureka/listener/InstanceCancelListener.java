package com.douqu.game.plantwar.eureka.listener;

import com.douqu.game.plantwar.core.utils.LogUtils;
import org.springframework.cloud.netflix.eureka.server.event.EurekaInstanceCanceledEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InstanceCancelListener implements ApplicationListener<EurekaInstanceCanceledEvent> {
    @Override
    public void onApplicationEvent(EurekaInstanceCanceledEvent event) {
        LogUtils.info("服务：" + event.getAppName() + ",挂了");
    }
}
