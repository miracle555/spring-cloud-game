package com.douqu.game.plantwar.test;

import com.douqu.game.plantwar.core.utils.HttpUtils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class TestMain {

    public static void main(String[] args) {

        final long start = System.currentTimeMillis();

        ExecutorService service = Executors.newCachedThreadPool();

        AtomicInteger j = new AtomicInteger(0);
        for (int i = 0; i < 500;i++){
            j.addAndGet(1);
            service.execute(new Thread(()->{
                if (j.get() == 1){
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                for (int z = 0 ; z < 5; z ++ ){
                    String result =  HttpUtils.httpPost("http://192.168.2.234:8202/plantwar/test/test","");
                    System.out.println(result);
                }

                j.set(j.get() - 1);
                if (j.get() == 0){
                    //所有的线程执行完毕
                    long end = System.currentTimeMillis();
                    //200毫秒睡的去掉
                    System.out.println("共用时 ->"  + (end - start - 200));
                }
            }));
        }
    }
}
