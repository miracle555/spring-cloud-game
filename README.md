# spring-cloud-game

#### 项目介绍
基于springboot,redis,springcloud,webflux,rabbitmq的一款小游戏架构

#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2018/0922/171348_64a5654c_1586982.png "图片3.png")

2018/9/22新增data服务器
 Data服务器负责数据定时处理数据，将redis中的数据同步到mysql数据库，后期包括很多定时任务或者日志系统通过rabbitmq移植到data服务器，因为auth与main服务器本身可能因为负载的原因存在多个所以并不适合进行定时任务处理。。。
Data服务器只可以启动一个 不然会造成数据重复处理

使用spring cloud的eureka作为服务治理中心，zuul作为api网关。 nginx实现zuul网关的负载均衡。 auth认证服务与main逻辑服通过zuul实现转发与负载均衡。
auth 采用springboot-start-web启动，实现登录等IO操作，并保存到redis内存数据库中。 main 采用springboot-start-webflux启动，直接操作redis。在auth中定时同步redis中的数据到mysql。

什么是微服务？
微服务是将整个web应用拆分成一系列小的web服务。这些web服务可以独立的编译部署，并通过各自暴露的api接口进行通信。它们之间彼此协作，作为一个整体为用户提供服务，却可以独立的扩展。
一、spring cloud
Spring cloud是一系列框架的有序集合，它利用spring boot的开发便利性巧妙的简化了分布式系统基础设施的开发，如服务发现注册、配置中心、消息总线、负载均衡、熔断器、数据监控等。Spring cloud并没有重复造轮子，它只是将目前各家公司开发的比较成熟、经得起实际考研的服务框架整合起来。（摘自百度百科）
主要组件：https://blog.csdn.net/yejingtao703/article/details/78331442/
这篇文章比喻的非常之好，spring cloud的组件可以分为两大类，润物无声类和独挑大梁类。
润物无声类：
Ribbon：客户端负载均衡，特性有区域亲和，重试机制
在我们项目中各个服务之间暂时不存在相互调用，所以未采用
Hystrix：熔断器，客户端容错保护，特性有服务降级，服务熔断，请求缓存，请求合并，依赖隔离
熔断器是为了保护ribbon，所以我们也没有采用...
Feign：声明式服务调用，本质上就是ribbon + hystrix
Stream：消息驱动，有sink source processor三种通道，特性有订阅发布，消费组，消息分区
Bus:消息总线，配合config仓库修改的一种stream实现
由于时间和人员配比，项目规模等原因在我们项目中暂时没有采用config作为配置中心，所以未用到bus

独挑大梁类：
Eureka：服务注册中心，特性有失效剔除，服务保护。我们的所有服务都注册到eureka上，可以使eureka的ip:port 查看详细信息
Dashboard：hystrix仪表盘，监控集群模式和单点模式。其中集群模式需要收集器turbine配合
我们项目不算严格的微服务没有那么多功能也没有拆分的太细，所以没有用到
Zuul：api网关服务，功能有路由分发和过滤
在我们的项目中将auth和main服务配置到zuul中实现路由的转发与负载，可以使auth与main的ip与端口不必暴露给客户端
Config：分布式配置中心，支持本地仓库、svn、git、jar包内配置等模式，我们项目中并未引入该组件
二、Nginx
nginx 是一个高性能的http和反向代理服务，在我们项目中通过nginx对zuul网关的负载，使我们的项目可以承载更大的用户量
三、Redis
Redis是一款nosql非关系型数据库。Redis是一个key-value存储系统，和memcached类似，value支持多种类型，包括string，list，set，zset和hash不使用集群的情况下redis本身是线程安全的。
在我们的项目中导入springboot的data-redis包进行redis的操作，auth与main连接同一个redis数据库作为缓存，实现不同服务之间的内存共享，
Key 我们使用的是用户的roleId下划线加 value对应的实体类的classname
Value 为了增加可读性 我们使用的是json类型的string


四、Webflux
Webflux是基于reactor实现的，在spring boot2.0和spring5中新增加的模块。Webflux模块包含对响应式http和websocket客户端的支持，以及对rest，html和websocket交互等程序的支持，一般来说springmvc用于异步处理，spring webflux用于异步处理
Reactor设计模式一种事件驱动的实现方式，在处理并发请求服务端的场景有很好的应用，我们常用的netty框架就是基于reactor模式实现的。
Reactor主要包括以下几个组件：

![输入图片说明](https://images.gitee.com/uploads/images/2018/0922/145123_bbf702ef_1586982.png "图片2.png")
在我们项目中main服务器就是使用webflux进行开发的，值得注意的是在使用webflux的时候所有的响应一定要快速的响应，如果阻塞的时间过长可能会造成整个系统的崩溃，所以我们在main服务器中禁止直接操作mysql，都是直接操作redis中的缓存

五、主要模块介绍


我们这里通过两个请求来更好的理解各个模块之间的关系
![输入图片说明](https://images.gitee.com/uploads/images/2018/0922/145135_f327bc8f_1586982.png "图片1.png")

登录请求：当用户发送发送来请求的时候我们首先经过nginx的负载均衡转发到zuul，然后根据zuul中的配置根据请求的url路由转发到zuth服务器，在auth服务器中我们首先进行第四步去redis中查找玩家的缓存，如果缓存不存在我们进行第五步去mysql中找个这个玩家，并进行第六步把玩家信息放入redis缓存中然后根据玩家信息生成token返回给用户
初始化用户信息请求：与登录一样首先通过nginx与zuul 路由转发到main服务器，然后直接去redis中取玩家信息，返回给用户

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)