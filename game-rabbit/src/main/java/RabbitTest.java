import moreToMore.MoreSender;
import moreToMore.MoreSenderB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import singleToSingle.SingleSender;

@RestController
@RequestMapping("/rabbit")
public class RabbitTest {

    @Autowired
    private SingleSender singleSender;

    @Autowired
    private MoreSender moreSender;

    @Autowired
    private MoreSenderB moreSenderB;

    @PostMapping("/hello")
    public void hello(){
        singleSender.send();
    }

    @PostMapping("/more2more")
    public void more2more(){
        moreSender.send("hello more 1");
        moreSenderB.send("hello more B");
    }
}
