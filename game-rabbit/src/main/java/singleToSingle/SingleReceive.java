package singleToSingle;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "helloQueue")
public class SingleReceive {

    @RabbitHandler
    public void process(String hello){
        System.out.println("Single receive ->" + hello);
    }
}
