package moreToMore;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MoreSenderB {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String msg){
        String sendMsg = msg + new Date();
        System.out.println("moreSendA :" + sendMsg);
        this.amqpTemplate.convertAndSend("moreQueye" ,sendMsg);
    }
}
