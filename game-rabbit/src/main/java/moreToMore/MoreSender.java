package moreToMore;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MoreSender {

    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String msg){
        String sendMsg = msg + new Date();
        System.out.println("more send :" +sendMsg);
        this.amqpTemplate.convertAndSend("moreQueue",sendMsg);
    }
}
