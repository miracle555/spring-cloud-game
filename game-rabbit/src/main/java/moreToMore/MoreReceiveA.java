package moreToMore;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "moreQueue")
public class MoreReceiveA {

    @RabbitHandler
    public void process(String hello){
        System.out.println("moreReceiveA;" + hello);
    }
}
